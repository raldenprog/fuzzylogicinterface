﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace Fuzzy
{
    public class SQL_Helper
    {
        public Boolean CreateDataBaseFile()
        {
            string dbFileName = "fuzzy.db";
            if (!System.IO.File.Exists("fuzzy.db"))
            {
                SQLiteConnection.CreateFile(dbFileName);
                return true;
            }
            else return false;
        }

        void createTableVariables()
        {
            ExecuteNonQuery(@"CREATE TABLE IF NOT EXISTS `variables` (
	                                                        `id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	                                                        `name`	TEXT NOT NULL UNIQUE
                                                         );");
        }

        void createTableBounds()
        {
            ExecuteNonQuery(@"CREATE TABLE IF NOT EXISTS `bounds` (
	                                                                    `min`	NUMERIC,
	                                                                    `max`	NUMERIC
                                                                    );");
        }
        void createTableParametres()
        {
            ExecuteNonQuery(@"CREATE TABLE IF NOT EXISTS `parametres` (
	                                                                        `id`    integer,
	                                                                        `variable_id`	integer,
	                                                                        `parameter`	TEXT NOT NULL,
	                                                                        `start`	NUMERIC NOT NULL,
	                                                                        `end`	NUMERIC NOT NULL,
	                                                                        PRIMARY KEY(`id`),
	                                                                        FOREIGN KEY(`variable_id`) REFERENCES `variables`(`id`) ON DELETE CASCADE
                                                        );");
        }

        public void createTableRules()
        {
            ExecuteNonQuery(@"CREATE TABLE IF NOT EXISTS `rules` (
	                                                                       	`id`	integer,
	                                                                        `rule`	text,
	                                                                        `y`	text,
	                                                                        `n`	NUMERIC,
                                                                            `rule_share` TEXT,
                                                                            `condition`	TEXT,
	                                                                        PRIMARY KEY(`id`)
                                                                            );");
        }

        public void CreateDataBase()
        {
            if (CreateDataBaseFile() is true)
            {
                ExecuteNonQuery("PRAGMA foreign_keys = ON");
                createTableVariables();
                createTableParametres();
                createTableBounds();
                createTableRules();
            }
        }


        SQLiteConnection connectDataBase()
        {
            SQLiteConnection DB;
            DB = new SQLiteConnection("Data source=fuzzy.db; Version=3");
            DB.Open();
            return DB;
        }

        public SQLiteDataReader ExecuteReader(string query)
        {
            SQLiteConnection DB = connectDataBase();
            SQLiteCommand CMD = DB.CreateCommand();
            CMD.CommandText = query;
            SQLiteDataReader answer = CMD.ExecuteReader();
            return answer;
        }

        public void ExecuteNonQuery(string query)
        {
            SQLiteConnection DB = connectDataBase();
            SQLiteCommand CMD = DB.CreateCommand();
            CMD.CommandText = query;
            CMD.ExecuteNonQuery();
        }

        public int ExecuteScalar(string query)
        {
            SQLiteConnection DB = connectDataBase();
            SQLiteCommand CMD = DB.CreateCommand();
            CMD.CommandText = query;
            int answer = Convert.ToInt32(CMD.ExecuteScalar());
            return answer;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using Fuzzy;

namespace WindowsFormsApp1
{
    public partial class Rules : Form
    {
        private SQLiteConnection DB;
        List<ListBox> list_boxes = new List<ListBox>();



        public Rules()
        {
            InitializeComponent();
        }



        private void Rules_Load(object sender, EventArgs e)
        {
            var sql_helper = new SQL_Helper();
            ListBoxCreater();
            get_rules_from_db();

        }

        private void get_rules_from_db()
        {
            listBoxRules.Items.Clear();
            var sql_helper = new SQL_Helper();
            SQLiteDataReader answer = sql_helper.ExecuteReader("select * from rules");
            while (answer.Read())
            {
                listBoxRules.Items.Add(answer["condition"]);
            }
            
        }

        private void ListBoxCreater()
        {
            var sql_helper = new SQL_Helper();
            SQLiteDataReader answer = sql_helper.ExecuteReader("select name from variables where name != 'y' ");
            int list_size = sql_helper.ExecuteScalar("select count(name) from variables where name != 'y' ");
            
            int i = 0;
            int pos = 3;
            while (answer.Read())
            {
                i++;
                int vadiable_id = sql_helper.ExecuteScalar("select id from variables where name ='" + answer["name"].ToString() + "'");
                SQLiteDataReader parametres = sql_helper.ExecuteReader("select parameter from parametres where variable_id = "+ vadiable_id.ToString());
                ListBox lb = new ListBox();
                lb.Location = new Point(pos, 25);
                lb.Size = new Size(140, 150);
                while (parametres.Read())
                {
                    lb.Items.Add(parametres["parameter"]);
                }
                list_boxes.Add(lb);
                panel1.Controls.Add(lb);

                if (i == list_size)
                {
                    panel1.Controls.Add(new Label()
                    {
                        Name = i.ToString(),
                        Location = new Point(pos, 0),
                        Text = "                   " + answer["name"].ToString(),
                        AutoSize = true
                    });

                }
                else
                {
                    panel1.Controls.Add(new Label()
                    {
                        Name = i.ToString(),
                        Location = new Point(pos, 0),
                        Text = "                   " + answer["name"].ToString() + "                  and   ",
                        AutoSize = true
                    });
                    pos += 150;
                }
            }

            int y_id = sql_helper.ExecuteScalar("select id from variables where name = 'y'");
            SQLiteDataReader parametres_y = sql_helper.ExecuteReader("select parameter from parametres where variable_id = " + y_id.ToString());
            while (parametres_y.Read())
            {
                listBoxResult.Items.Add(parametres_y["parameter"]);
            }
        }



        private void ButtonDelete_Click(object sender, EventArgs e)
        {
            var sql_helper = new SQL_Helper();
            if (listBoxRules.SelectedIndex == -1)
            {
                MessageBox.Show("Выберите правило для удаления");
            }
            else
            {
                string rule = listBoxRules.Items[listBoxRules.SelectedIndex].ToString();
                sql_helper.ExecuteNonQuery("delete from rules where condition = '"+rule+"'");
                get_rules_from_db();
            }
        }

        private void LabelThen_Click(object sender, EventArgs e)
        {

        }


        private void Panel_Scroll()
        {
           
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            var sql_helper = new SQL_Helper();
            SQLiteDataReader variables = sql_helper.ExecuteReader("select name from variables where name != 'y'");
            string rule = "if   ";
            int i = 0;
            Console.WriteLine(variables);
            string rule_db = "";
            string rule_share = "";
            Boolean flag_do = true;
            while (variables.Read())
            {
                if (list_boxes[i].SelectedIndex == -1)
                {
                    //MessageBox.Show("Заполните все данные");
                    //flag_do = false;
                    //break;
                }
                else
                {
                    rule += "(" + variables["name"] + "  is  " + list_boxes[i].Items[list_boxes[i].SelectedIndex].ToString() + ")    and    ";
                    rule_db += list_boxes[i].Items[list_boxes[i].SelectedIndex].ToString() + " ";
                    rule_share += variables["name"] + ": " + list_boxes[i].Items[list_boxes[i].SelectedIndex].ToString() + "   ";
                    
                }

                i++;
            }

            
            string n = textBoxWeight.Text.Replace(".", ",");
            try
            {
                double n_checke = Convert.ToDouble(n);
            }
            catch
            {
                flag_do = false;

            }
            if (flag_do && listBoxResult.SelectedIndex != -1 && n != "")
            {
                n = n.Replace(",", ".");
                string y = listBoxResult.Items[listBoxResult.SelectedIndex].ToString();
                rule += "then  (" + listBoxResult.Items[listBoxResult.SelectedIndex].ToString() + ")" + " (" + n + ")";

                rule_share += "y: " + listBoxResult.Items[listBoxResult.SelectedIndex].ToString() + "   " + n;
                Console.WriteLine(rule_db);
                rule_db = rule_db.Remove(rule_db.Length - 1);
                rule_share = rule_share.Replace(",", ".");

                int check_rules = sql_helper.ExecuteScalar("select count(*) from rules where condition = '"+ rule + "'");
                if (check_rules == 0)
                {
                    sql_helper.ExecuteNonQuery("INSERT INTO rules (rule, y, n, rule_share, condition) VALUES  ('" + rule_db + "','" + y + "'," + n + ", '" + rule_share + "', '" + rule + "')");
                    get_rules_from_db();
                }
                else
                {
                    MessageBox.Show("Такое правило уже есть");
                }
            }
            else
            {
                MessageBox.Show("Заполните все данные корректно");
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            var sql_helper = new SQL_Helper();
            sql_helper.ExecuteNonQuery("delete from rules");
            get_rules_from_db();
        }
    }
}

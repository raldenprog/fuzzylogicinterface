﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fuzzy;
using System.Data.SQLite;



namespace WindowsFormsApp1
{
    public partial class VariablesForm : Form
    {
        string variable_id;
        DataGridView dataGridView1 = new DataGridView();
        public VariablesForm(string variable)
        {
            variable_id = variable;
            Console.WriteLine(variable);
            InitializeComponent();

            listBoxVariable.MouseDown += new MouseEventHandler(listBoxVariable_MouseDown);

        }

        private void Variables_Load(object sender, EventArgs e)
        {

            get_list();
        }

        void get_list()
        {
            var sql_helper = new SQL_Helper();
            SQLiteDataReader answer = sql_helper.ExecuteReader("select * from parametres where variable_id ='" + variable_id + "'");
            while (answer.Read())
            {
                string row_list = answer["parameter"] + " " + answer["start"] + " " + answer["end"];
                listBoxVariable.Items.Add(row_list);
            }
        }

        void listBoxVariable_MouseDown(object sender, MouseEventArgs e)
        {
            if (listBoxVariable.SelectedIndex != -1)
            {
                if (e.Button == MouseButtons.Right)
                {

                }
                if (e.Button == MouseButtons.Left)
                {
                    string variable_index = listBoxVariable.Items[listBoxVariable.SelectedIndex].ToString();
                    String[] words = variable_index.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    Console.WriteLine(words[0]);
                    Make_Graph(Convert.ToDouble(words[1]), Convert.ToDouble(words[2]));

                }
            }

        }

        void Make_Graph(double b, double c)
        {
            VariablesChart.Series[0].Points.Clear();
            VariablesChart.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;

            double start_y = 0.000002;
            double start = 0;
            while (start_y > 0.000001)
                {
                start_y = Math.Pow(Math.E, -((start - b) * (start - b)) / (2 * c * c));
                start -= 0.5;
            }
            double end = start * -1;
            
            
            Console.WriteLine(start);
            Console.WriteLine(end);
            for (double i = start; i < end; i+=0.5)
            {
                
                double y = Math.Pow(Math.E, -((i - b) * (i - b)) / (2 * c * c));
                //Console.Write(i);
                //Console.Write(" ");
                //Console.WriteLine(y);
                if (y > 0.0000001)
                {
                    VariablesChart.Series[0].Points.AddXY(i, y);
                }

            }
        }

        private void AddVariableBtn_Click(object sender, EventArgs e)
        {
            AddVariableForm AddForm = new AddVariableForm(variable_id);
            AddForm.Show();
            this.Close();
        }


        public void clear_list_box()
        {
            listBoxVariable.Items.Clear();
        }

        private void VariablesForm_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            var sql_helper = new SQL_Helper();
            string variable_index = listBoxVariable.Items[listBoxVariable.SelectedIndex].ToString();
            String[] words = variable_index.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            sql_helper.ExecuteNonQuery("delete from parametres where parameter='" + words[0] + "'");
            listBoxVariable.Items.RemoveAt(listBoxVariable.SelectedIndex);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fuzzy;
using System.Data.SQLite;

namespace WindowsFormsApp1
{
    public partial class AddVariableForm : Form
    {
        string variable_id;
        public AddVariableForm(string variable)
        {
            variable_id = variable;
            InitializeComponent();
        }

        private void AddVariable_Load(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void DispalyGraph_Click(object sender, EventArgs e)
        {
            string name = textBoxParameter.Text;
            double b = Convert.ToDouble(textBoxStart.Text.Replace(".", ","));
            double c = Convert.ToDouble(textBoxEnd.Text.Replace(".", ","));
            VariablesChart.Series[0].Points.Clear();
            VariablesChart.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;

            double start_y = 0.000002;
            double start = 0;
            while (start_y > 0.000001)
            {
                start_y = Math.Pow(Math.E, -((start - b) * (start - b)) / (2 * c * c));
                start -= 0.5;
            }
            double end = start * -1;


            Console.WriteLine(start);
            Console.WriteLine(end);
            for (double i = start; i < end; i += 0.5)
            {

                double y = Math.Pow(Math.E, -((i - b) * (i - b)) / (2 * c * c));
                //Console.Write(i);
                //Console.Write(" ");
                //Console.WriteLine(y);
                if (y > 0.0000001)
                {
                    VariablesChart.Series[0].Points.AddXY(i, y);
                }

            }

        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            string parameter = textBoxParameter.Text;
            string start = textBoxStart.Text.Replace(",", ".");
            string end = textBoxEnd.Text.Replace(",", ".");
            var sql_helper = new SQL_Helper();
            sql_helper.ExecuteNonQuery("INSERT INTO parametres (variable_id, parameter, start, end) VALUES  ('" + variable_id + "', '" + parameter + "', " + start + ", " + end + ")");
            this.Close();
        }

        private void AddVariableForm_FormClosed(object sender, FormClosedEventArgs e)
        {

            VariablesForm ifrm = new VariablesForm(variable_id);
            ifrm.Show();
        }
    }
}

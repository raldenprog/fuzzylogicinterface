﻿namespace WindowsFormsApp1
{
    partial class VariablesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.VariablesChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.listBoxVariable = new System.Windows.Forms.ListBox();
            this.AddVariableBtn = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.VariablesChart)).BeginInit();
            this.SuspendLayout();
            // 
            // VariablesChart
            // 
            chartArea1.Name = "ChartArea1";
            this.VariablesChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.VariablesChart.Legends.Add(legend1);
            this.VariablesChart.Location = new System.Drawing.Point(365, 8);
            this.VariablesChart.Margin = new System.Windows.Forms.Padding(2);
            this.VariablesChart.Name = "VariablesChart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.VariablesChart.Series.Add(series1);
            this.VariablesChart.Size = new System.Drawing.Size(390, 265);
            this.VariablesChart.TabIndex = 18;
            this.VariablesChart.Text = "chart1";
            // 
            // listBoxVariable
            // 
            this.listBoxVariable.FormattingEnabled = true;
            this.listBoxVariable.Location = new System.Drawing.Point(8, 8);
            this.listBoxVariable.Margin = new System.Windows.Forms.Padding(2);
            this.listBoxVariable.Name = "listBoxVariable";
            this.listBoxVariable.Size = new System.Drawing.Size(307, 264);
            this.listBoxVariable.TabIndex = 19;
            // 
            // AddVariableBtn
            // 
            this.AddVariableBtn.Location = new System.Drawing.Point(13, 296);
            this.AddVariableBtn.Name = "AddVariableBtn";
            this.AddVariableBtn.Size = new System.Drawing.Size(111, 26);
            this.AddVariableBtn.TabIndex = 20;
            this.AddVariableBtn.Text = "Добавить";
            this.AddVariableBtn.UseVisualStyleBackColor = true;
            this.AddVariableBtn.Click += new System.EventHandler(this.AddVariableBtn_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(198, 296);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 21;
            this.buttonDelete.Text = "Удалить";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // VariablesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(763, 456);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.AddVariableBtn);
            this.Controls.Add(this.listBoxVariable);
            this.Controls.Add(this.VariablesChart);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "VariablesForm";
            this.Text = "Variables";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.VariablesForm_FormClosed);
            this.Load += new System.EventHandler(this.Variables_Load);
            ((System.ComponentModel.ISupportInitialize)(this.VariablesChart)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart VariablesChart;
        private System.Windows.Forms.ListBox listBoxVariable;
        private System.Windows.Forms.Button AddVariableBtn;
        private System.Windows.Forms.Button buttonDelete;
    }
}
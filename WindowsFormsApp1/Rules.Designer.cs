﻿namespace WindowsFormsApp1
{
    partial class Rules
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxRules = new System.Windows.Forms.ListBox();
            this.labelThen = new System.Windows.Forms.Label();
            this.listBoxResult = new System.Windows.Forms.ListBox();
            this.labelWeight = new System.Windows.Forms.Label();
            this.textBoxWeight = new System.Windows.Forms.TextBox();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelIf = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxRules
            // 
            this.listBoxRules.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxRules.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Italic);
            this.listBoxRules.FormattingEnabled = true;
            this.listBoxRules.ItemHeight = 19;
            this.listBoxRules.Location = new System.Drawing.Point(9, 10);
            this.listBoxRules.Margin = new System.Windows.Forms.Padding(2);
            this.listBoxRules.Name = "listBoxRules";
            this.listBoxRules.Size = new System.Drawing.Size(1002, 194);
            this.listBoxRules.TabIndex = 0;
            // 
            // labelThen
            // 
            this.labelThen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.labelThen.AutoSize = true;
            this.labelThen.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Italic);
            this.labelThen.Location = new System.Drawing.Point(917, 216);
            this.labelThen.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelThen.Name = "labelThen";
            this.labelThen.Size = new System.Drawing.Size(41, 19);
            this.labelThen.TabIndex = 1;
            this.labelThen.Text = "Then";
            this.labelThen.Click += new System.EventHandler(this.LabelThen_Click);
            // 
            // listBoxResult
            // 
            this.listBoxResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxResult.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Italic);
            this.listBoxResult.FormattingEnabled = true;
            this.listBoxResult.ItemHeight = 19;
            this.listBoxResult.Location = new System.Drawing.Point(869, 241);
            this.listBoxResult.Margin = new System.Windows.Forms.Padding(2);
            this.listBoxResult.Name = "listBoxResult";
            this.listBoxResult.Size = new System.Drawing.Size(140, 137);
            this.listBoxResult.TabIndex = 3;
            // 
            // labelWeight
            // 
            this.labelWeight.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.labelWeight.AutoSize = true;
            this.labelWeight.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Italic);
            this.labelWeight.Location = new System.Drawing.Point(877, 385);
            this.labelWeight.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelWeight.Name = "labelWeight";
            this.labelWeight.Size = new System.Drawing.Size(37, 19);
            this.labelWeight.TabIndex = 4;
            this.labelWeight.Text = "Вес:";
            // 
            // textBoxWeight
            // 
            this.textBoxWeight.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.textBoxWeight.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Italic);
            this.textBoxWeight.Location = new System.Drawing.Point(933, 381);
            this.textBoxWeight.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxWeight.Name = "textBoxWeight";
            this.textBoxWeight.Size = new System.Drawing.Size(56, 27);
            this.textBoxWeight.TabIndex = 5;
            // 
            // buttonDelete
            // 
            this.buttonDelete.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonDelete.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Italic);
            this.buttonDelete.Location = new System.Drawing.Point(461, 412);
            this.buttonDelete.Margin = new System.Windows.Forms.Padding(2);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(109, 27);
            this.buttonDelete.TabIndex = 6;
            this.buttonDelete.Text = "Удалить";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.ButtonDelete_Click);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.buttonAdd.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Italic);
            this.buttonAdd.Location = new System.Drawing.Point(574, 412);
            this.buttonAdd.Margin = new System.Windows.Forms.Padding(2);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(108, 27);
            this.buttonAdd.TabIndex = 7;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.labelIf);
            this.panel1.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Italic);
            this.panel1.Location = new System.Drawing.Point(11, 216);
            this.panel1.Margin = new System.Windows.Forms.Padding(2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(849, 192);
            this.panel1.TabIndex = 10;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // labelIf
            // 
            this.labelIf.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelIf.AutoSize = true;
            this.labelIf.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Italic);
            this.labelIf.Location = new System.Drawing.Point(2, -4);
            this.labelIf.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelIf.Name = "labelIf";
            this.labelIf.Size = new System.Drawing.Size(18, 19);
            this.labelIf.TabIndex = 0;
            this.labelIf.Text = "if";
            // 
            // btnClear
            // 
            this.btnClear.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.btnClear.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Italic);
            this.btnClear.Location = new System.Drawing.Point(17, 414);
            this.btnClear.Margin = new System.Windows.Forms.Padding(2);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(108, 27);
            this.btnClear.TabIndex = 11;
            this.btnClear.Text = "СБРОС";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // Rules
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 449);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.textBoxWeight);
            this.Controls.Add(this.labelWeight);
            this.Controls.Add(this.listBoxResult);
            this.Controls.Add(this.labelThen);
            this.Controls.Add(this.listBoxRules);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Rules";
            this.Text = "Rules";
            this.Load += new System.EventHandler(this.Rules_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxRules;
        private System.Windows.Forms.Label labelThen;
        private System.Windows.Forms.ListBox listBoxResult;
        private System.Windows.Forms.Label labelWeight;
        private System.Windows.Forms.TextBox textBoxWeight;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonAdd;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelIf;
        private System.Windows.Forms.Button btnClear;
    }
}
﻿namespace WindowsFormsApp1
{
    partial class AddVariableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.textBoxParameter = new System.Windows.Forms.TextBox();
            this.VariablesChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.textBoxStart = new System.Windows.Forms.TextBox();
            this.textBoxEnd = new System.Windows.Forms.TextBox();
            this.DispalyGraph = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.VariablesChart)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxParameter
            // 
            this.textBoxParameter.Location = new System.Drawing.Point(12, 12);
            this.textBoxParameter.Multiline = true;
            this.textBoxParameter.Name = "textBoxParameter";
            this.textBoxParameter.Size = new System.Drawing.Size(354, 37);
            this.textBoxParameter.TabIndex = 0;
            // 
            // VariablesChart
            // 
            chartArea1.Name = "ChartArea1";
            this.VariablesChart.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.VariablesChart.Legends.Add(legend1);
            this.VariablesChart.Location = new System.Drawing.Point(399, 12);
            this.VariablesChart.Margin = new System.Windows.Forms.Padding(2);
            this.VariablesChart.Name = "VariablesChart";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.VariablesChart.Series.Add(series1);
            this.VariablesChart.Size = new System.Drawing.Size(390, 265);
            this.VariablesChart.TabIndex = 19;
            this.VariablesChart.Text = "chart1";
            // 
            // textBoxStart
            // 
            this.textBoxStart.Location = new System.Drawing.Point(54, 72);
            this.textBoxStart.Name = "textBoxStart";
            this.textBoxStart.Size = new System.Drawing.Size(105, 20);
            this.textBoxStart.TabIndex = 20;
            // 
            // textBoxEnd
            // 
            this.textBoxEnd.Location = new System.Drawing.Point(273, 72);
            this.textBoxEnd.Name = "textBoxEnd";
            this.textBoxEnd.Size = new System.Drawing.Size(93, 20);
            this.textBoxEnd.TabIndex = 21;
            this.textBoxEnd.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // DispalyGraph
            // 
            this.DispalyGraph.Location = new System.Drawing.Point(215, 117);
            this.DispalyGraph.Name = "DispalyGraph";
            this.DispalyGraph.Size = new System.Drawing.Size(151, 23);
            this.DispalyGraph.TabIndex = 23;
            this.DispalyGraph.Text = "Отобразить на графике";
            this.DispalyGraph.UseVisualStyleBackColor = true;
            this.DispalyGraph.Click += new System.EventHandler(this.DispalyGraph_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Location = new System.Drawing.Point(39, 117);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 24;
            this.buttonSave.Text = "Сохранить";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // AddVariableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 321);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.DispalyGraph);
            this.Controls.Add(this.textBoxEnd);
            this.Controls.Add(this.textBoxStart);
            this.Controls.Add(this.VariablesChart);
            this.Controls.Add(this.textBoxParameter);
            this.Name = "AddVariableForm";
            this.Text = "AddVariable";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AddVariableForm_FormClosed);
            this.Load += new System.EventHandler(this.AddVariable_Load);
            ((System.ComponentModel.ISupportInitialize)(this.VariablesChart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxParameter;
        private System.Windows.Forms.DataVisualization.Charting.Chart VariablesChart;
        private System.Windows.Forms.TextBox textBoxStart;
        private System.Windows.Forms.TextBox textBoxEnd;
        private System.Windows.Forms.Button DispalyGraph;
        private System.Windows.Forms.Button buttonSave;
    }
}
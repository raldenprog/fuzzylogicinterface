﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using Fuzzy;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        private SQLiteConnection DB;
        List<string> nameRules = new List<string>();

        public Form2()
        {
            InitializeComponent();
        }

        public Form2(Form1 f)
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            var sql_helper = new SQL_Helper();
            dataGridViewRules.AutoResizeColumnHeadersHeight();

            SQLiteDataReader answer = sql_helper.ExecuteReader("select name from variables");
            while (answer.Read())
            {
                nameRules.Add(answer["name"].ToString());
                DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn
                {
                    Name = answer["name"].ToString(),
                    HeaderText = answer["name"].ToString()
                };
                dataGridViewRules.Columns.AddRange(column);
                this.Controls.Add(dataGridViewRules);
                
               
            }

            DataGridViewTextBoxColumn column_n = new DataGridViewTextBoxColumn
            {
                Name = "n",
                HeaderText = "n"
            };
            dataGridViewRules.Columns.AddRange(column_n);
            this.Controls.Add(dataGridViewRules);

            SQLiteDataReader rules = sql_helper.ExecuteReader("select * from rules");
            while (rules.Read())
            {
                string[] list_rules = rules["rule"].ToString().Split(' ');
                DataGridViewRow row0 = new DataGridViewRow();

                foreach (string r in list_rules)
                {
                    DataGridViewTextBoxCell rule = new DataGridViewTextBoxCell
                    {
                        Value = r
                    };
                    row0.Cells.Add(rule);
                }

                DataGridViewTextBoxCell col_y = new DataGridViewTextBoxCell
                {
                    Value = rules["y"]
                };
                row0.Cells.Add(col_y);

                DataGridViewTextBoxCell col_n = new DataGridViewTextBoxCell
                {
                    Value = rules["n"]
                };
                row0.Cells.Add(col_n);

                dataGridViewRules.Rows.Add(row0);
            }

        }

        private void BtnSaveRules_Click(object sender, EventArgs e)
        {
            var sql_helper = new SQL_Helper();
            sql_helper.ExecuteNonQuery("delete from rules");
            nameRules.Add("y");
            nameRules.Add("n");
            for (int i = 0; i < dataGridViewRules.Rows.Count - 1; i++)
            {
                string nameValues = "";
                string values_share = "";
                for (int j = 0; j < dataGridViewRules.ColumnCount-2; j++)
                {
                    nameValues = nameValues + dataGridViewRules.Rows[i].Cells[j].Value.ToString() + " ";
                }
                for (int j = 0; j < dataGridViewRules.ColumnCount-1; j++)
                {
                    values_share = values_share + nameRules[j] + ": " + dataGridViewRules.Rows[i].Cells[j].Value.ToString() + "   ";
                }
                values_share = values_share + dataGridViewRules.Rows[i].Cells[dataGridViewRules.ColumnCount-1].Value.ToString();
                nameValues = nameValues.Remove(nameValues.Length - 1);
                string y = dataGridViewRules.Rows[i].Cells[dataGridViewRules.ColumnCount - 2].Value.ToString();
                string n = dataGridViewRules.Rows[i].Cells[dataGridViewRules.ColumnCount - 1].Value.ToString();
                string n_touch = n.Replace(",", ".");
                values_share = values_share.Replace(",", ".");
                sql_helper.ExecuteNonQuery("INSERT INTO rules (rule, y, n, rule_share) VALUES  ('" + nameValues + "','" + y + "'," + n_touch +", '"+ values_share + "')");

            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Data.SQLite;
using Fuzzy;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private SQLiteConnection DB;
        Form2 frm2;

        List<string> nameRules = new List<string>();
        List<string> columnsValues = new List<string>();
        DataGridView dataGridView1 = new DataGridView();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var sql_helper = new SQL_Helper();
            sql_helper.CreateDataBase();
            //AddGrid();
            listBoxVariables.MouseDown += new MouseEventHandler(ListBoxVariables_MouseDown);
            ReadTextBox();
            GridValues();
            SQLiteDataReader answer = sql_helper.ExecuteReader("select * from variables");
            while (answer.Read())
            {
                listBoxVariables.Items.Add(answer["name"]);
            }

            Console.WriteLine(Graph(1, 2, 3));

        }

        private double Graph(double x, double c, double b)
        {
            return Math.Pow(Math.E, -((x - b) * (x - b)) / (2 * c * c));
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void ListBoxVariables_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private static DialogResult ShowInputDialog(ref string input, string title)
        {
            System.Drawing.Size size = new System.Drawing.Size(350, 70);
            Form inputBox = new Form
            {
                FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog,
                ClientSize = size,
                Text = title
            };

            System.Windows.Forms.TextBox textBox = new TextBox
            {
                Size = new System.Drawing.Size(size.Width - 10, 23),
                Location = new System.Drawing.Point(5, 5),
                Text = input
            };
            inputBox.Controls.Add(textBox);

            Button okButton = new Button
            {
                DialogResult = System.Windows.Forms.DialogResult.OK,
                Name = "okButton",
                Size = new System.Drawing.Size(75, 23),
                Text = "&OK",
                Location = new System.Drawing.Point(size.Width - 80 - 80, 39)
            };
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button
            {
                DialogResult = System.Windows.Forms.DialogResult.Cancel,
                Name = "cancelButton",
                Size = new System.Drawing.Size(75, 23),
                Text = "&Cancel",
                Location = new System.Drawing.Point(size.Width - 80, 39)
            };
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;

            DialogResult result = inputBox.ShowDialog();
            input = textBox.Text;
            return result;
        }

        private void AddVariableButton_Click(object sender, EventArgs e)
        {
            var sql_helper = new SQL_Helper();
            string input = "Название переменной";
            if (DialogResult.OK == ShowInputDialog(ref input, "Новая переменная"))
            {
                sql_helper.ExecuteNonQuery("drop table if exists rules");
                listBoxVariables.Items.Add(input);
                sql_helper.ExecuteNonQuery("insert into variables(name) values('" + input + "')");
                sql_helper.createTableRules();
                ClearGrid();
                GridValues();

            }

        }

        private void ChangeVariableButton_Click(object sender, EventArgs e)
        {

            string variable_id = Get_variable_id(listBoxVariables.Items[listBoxVariables.SelectedIndex].ToString());
            VariablesForm VF = new VariablesForm(variable_id);
            VF.Show();
            /*
            string oldName = listBoxVariables.Items[listBoxVariables.SelectedIndex].ToString();
            string input = listBoxVariables.Items[listBoxVariables.SelectedIndex].ToString();
            if (DialogResult.OK == ShowInputDialog(ref input, "Редактировать переменную"))
                listBoxVariables.Items[listBoxVariables.SelectedIndex] = input;
            string newName = input;
            var sql_helper = new SQL_Helper();
            sql_helper.ExecuteNonQuery("UPDATE variables SET name = '" + newName + "' WHERE name = '" + oldName + "'");
            ClearGrid();
            GridValues();
            */
        }

        private void DeleteVariableButton_Click(object sender, EventArgs e)
        {
            var sql_helper = new SQL_Helper();
            sql_helper.ExecuteNonQuery("drop table if exists rules");
            sql_helper.ExecuteNonQuery("delete from variables where name='" + listBoxVariables.Items[listBoxVariables.SelectedIndex].ToString() + "'");
            listBoxVariables.Items.RemoveAt(listBoxVariables.SelectedIndex);
            sql_helper.createTableRules();
            ClearGrid();
            GridValues();

        }

        void ListBoxVariables_MouseDown(object sender, MouseEventArgs e)
        {
            if (listBoxVariables.SelectedIndex != -1)
            {
                if (e.Button == MouseButtons.Right)
                {
                    
                }
                if (e.Button == MouseButtons.Left)
                {
                    
                    //dataGridView1.Rows.Clear();
                    //string variable_id = Get_variable_id(listBoxVariables.Items[listBoxVariables.SelectedIndex].ToString());
                    //VariablesForm VF = new VariablesForm(variable_id);
                    //VF.Show();
                    //var sql_helper = new SQL_Helper();
                    //SQLiteDataReader answer = sql_helper.ExecuteReader("select * from parametres where variable_id ='" + variable_id + "'");
                    /*
                    while (answer.Read())
                    {
                        DataGridViewCell parameter = new DataGridViewTextBoxCell();
                        DataGridViewCell start = new DataGridViewTextBoxCell();
                        DataGridViewTextBoxCell end = new DataGridViewTextBoxCell();
                        parameter.Value = answer["parameter"];
                        start.Value = answer["start"];
                        end.Value = answer["end"];
                        DataGridViewRow row0 = new DataGridViewRow();
                        row0.Cells.AddRange(parameter, start, end);
                        dataGridView1.Rows.Add(row0);
                    }
                    */

                }
            }
            
        }
        
        void AddGrid()
        {
            listBoxVariables.MouseDown += new MouseEventHandler(ListBoxVariables_MouseDown);
            dataGridView1.Size = new Size(343, 290);
            dataGridView1.Location = new Point(372, 32);
            DataGridViewTextBoxColumn columnParam = new DataGridViewTextBoxColumn
            {
                Name = "param",
                HeaderText = "PARAM"
            };

            DataGridViewTextBoxColumn columnStart = new DataGridViewTextBoxColumn
            {
                Name = "start",
                HeaderText = "START"
            };

            DataGridViewTextBoxColumn columnEnd = new DataGridViewTextBoxColumn
            {
                Name = "end",
                HeaderText = "END"
            };
            dataGridView1.Columns.AddRange(columnParam, columnStart, columnEnd);
            this.Controls.Add(dataGridView1);
            dataGridView1.BackgroundColor = SystemColors.ControlDarkDark;
        }

        public string Get_variable_id(string variable)
        {
            var sql_helper = new SQL_Helper();
            SQLiteDataReader ans = sql_helper.ExecuteReader("select id from variables where name ='" + variable + "'");
            string variable_id = "";
            while (ans.Read())
            {
                variable_id = ans["id"].ToString();
            }

            return variable_id;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            var sql_helper = new SQL_Helper();
            string variable = listBoxVariables.Items[listBoxVariables.SelectedIndex].ToString();
            string variable_id = Get_variable_id(variable);
            sql_helper.ExecuteNonQuery("delete from parametres where variable_id = '" + variable_id + "'");

            for (int i = 0; i < dataGridView1.Rows.Count-1; i++)
            {
                string parameter = dataGridView1.Rows[i].Cells[0].Value.ToString();
                string start = dataGridView1.Rows[i].Cells[1].Value.ToString().Replace(",", ".");
                string end = dataGridView1.Rows[i].Cells[2].Value.ToString().Replace(",", ".");
                sql_helper.ExecuteNonQuery("INSERT INTO parametres (variable_id, parameter, start, end) VALUES  ('" + variable_id + "', '" + parameter + "', " + start + ", " + end + ")");
            }
        }

        private void BtnRules_Click(object sender, EventArgs e)
        {
            Form2 newForm = new Form2();
            newForm.Show();
        }

        void WriteTextBox()
        {
            var sql_helper = new SQL_Helper();
            string min = txtBoxMin.Text;
            string max = txtBoxMax.Text;
            sql_helper.ExecuteNonQuery("delete from bounds");
            sql_helper.ExecuteNonQuery("INSERT INTO bounds (min, max) VALUES  (" + min + "," + max + ")");
        }

        void ReadTextBox()
        {
            var sql_helper = new SQL_Helper();
            SQLiteDataReader answer = sql_helper.ExecuteReader("select * from bounds");
            while (answer.Read())
            {
                txtBoxMin.Text = answer["min"].ToString();
                txtBoxMax.Text = answer["max"].ToString();
            }
        }

        string DatabaseToString()
        {
            var sql_helper = new SQL_Helper();
            string variablesAndRules = "";
            SQLiteDataReader answer = sql_helper.ExecuteReader("select name from variables");
            while (answer.Read())
            {
                nameRules.Add(answer["name"].ToString());
                variablesAndRules = variablesAndRules + "[" + answer["name"].ToString() + "] ";
                string variable_id = Get_variable_id(answer["name"].ToString());
                
                SQLiteDataReader answerParametres = sql_helper.ExecuteReader("select * from parametres where variable_id = '" + variable_id + "'");
                while (answerParametres.Read())
                {
                    variablesAndRules = variablesAndRules + answerParametres["parameter"].ToString() + ":  " + answerParametres["start"].ToString() +
                        " " + answerParametres["end"].ToString() + "   ";
                }
                variablesAndRules += "\n";

            }
            
            variablesAndRules = variablesAndRules.Remove(variablesAndRules.Length - 2);
            SQLiteDataReader answerBounds = sql_helper.ExecuteReader("select * from bounds");
            while (answerBounds.Read())
            {
                txtBoxMin.Text = answerBounds["min"].ToString();
                txtBoxMax.Text = answerBounds["max"].ToString();
                variablesAndRules = variablesAndRules+"MIN: " + answerBounds["min"].ToString() + "  MAX: " + answerBounds["max"].ToString() + "\n\n[RULES]\n";
            }
            
            SQLiteDataReader rules = sql_helper.ExecuteReader("select rule_share from rules");

            nameRules.Add("n");
            while (rules.Read())
            {
                variablesAndRules += rules["rule_share"];
                variablesAndRules += "\n";
            }
            variablesAndRules = variablesAndRules.Remove(variablesAndRules.Length - 1);
            variablesAndRules = variablesAndRules.Replace(",", ".");
            Console.WriteLine(variablesAndRules);
            return variablesAndRules;
            
        }


        void GridValues()
        {
            var sql_helper = new SQL_Helper();
            SQLiteDataReader answer = sql_helper.ExecuteReader("select name from variables");
            if (answer.HasRows)
            { 
                while (answer.Read())
                {   if (answer["name"].ToString() != "y" && answer["name"].ToString() != "Y")
                    {
                        columnsValues.Add(answer["name"].ToString());
                        DataGridViewTextBoxColumn column = new DataGridViewTextBoxColumn
                        {
                            Name = answer["name"].ToString(),
                            HeaderText = answer["name"].ToString()
                        };
                        gridResults.Columns.AddRange(column);
                        this.Controls.Add(gridResults);
                    }

                }
                try
                {
                    gridResults.Rows.Add();
                }
                catch
                {

                }

            }
          
        }

        void ClearGrid()
        {
            foreach(string cv in columnsValues)
            {
               if (cv != "y" || cv != "Y")
                {
                    try
                    {
                        gridResults.Columns.Remove(cv);

                    }
                    catch
                    {

                    }
                }

            }
        }

        string ReadValues()
        {
            string values = "";
            for (int i = 0; i < gridResults.Rows.Count; i++)
            {
                for (int j = 0; j < gridResults.ColumnCount; j++)
                {
                    values += nameRules[j].ToString() + ": " + gridResults.Rows[i].Cells[j].Value.ToString() + " ";
                }
                
            }

            return values;
        }


        private void BtnCalc_Click(object sender, EventArgs e)
        {
            WriteTextBox();
            var calculator = new Mamdani();

            string VarsAndRules = DatabaseToString();
            

            Parser.Parse(calculator, VarsAndRules);

            //string valuesString = "x1: -2  x2: 7  x3: -8  x4: 1.75  x5: 17";

            string valuesString = ReadValues();
            var values = Parser.ParseVarValues(calculator, valuesString);
            var results = calculator.Calculate(values).ToArray();
            tbResult.Clear();
            for (int i = 0; i < calculator.OutputVariables.Count; i++)
            {
                tbResult.AppendText(calculator.OutputVariables[i].Name + ": " + results[i].ToString("0.00", CultureInfo.InvariantCulture) + "\r\n");
            }

        }

        private void BtnCreateRules_Click(object sender, EventArgs e)
        {
            Rules newForm = new Rules();
            newForm.Show();
            
        }

        private void chart1_Click(object sender, EventArgs e)
        {

        }

        private void VariablesBtn_Click(object sender, EventArgs e)
        {
        }
    }
}

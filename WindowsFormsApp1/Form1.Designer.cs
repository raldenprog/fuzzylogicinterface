﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.AddVariableButton = new System.Windows.Forms.Button();
            this.ChangeVariableButton = new System.Windows.Forms.Button();
            this.DeleteVariableButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.listBoxVariables = new System.Windows.Forms.ListBox();
            this.btnCalc = new System.Windows.Forms.Button();
            this.txtBoxMin = new System.Windows.Forms.TextBox();
            this.txtBoxMax = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.gridResults = new System.Windows.Forms.DataGridView();
            this.tbResult = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCreateRules = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.gridResults)).BeginInit();
            this.SuspendLayout();
            // 
            // AddVariableButton
            // 
            this.AddVariableButton.AutoSize = true;
            this.AddVariableButton.Location = new System.Drawing.Point(13, 272);
            this.AddVariableButton.Margin = new System.Windows.Forms.Padding(4);
            this.AddVariableButton.Name = "AddVariableButton";
            this.AddVariableButton.Size = new System.Drawing.Size(133, 39);
            this.AddVariableButton.TabIndex = 1;
            this.AddVariableButton.Text = "Добавить";
            this.AddVariableButton.UseVisualStyleBackColor = true;
            this.AddVariableButton.Click += new System.EventHandler(this.AddVariableButton_Click);
            // 
            // ChangeVariableButton
            // 
            this.ChangeVariableButton.AutoSize = true;
            this.ChangeVariableButton.Location = new System.Drawing.Point(132, 272);
            this.ChangeVariableButton.Margin = new System.Windows.Forms.Padding(4);
            this.ChangeVariableButton.Name = "ChangeVariableButton";
            this.ChangeVariableButton.Size = new System.Drawing.Size(194, 39);
            this.ChangeVariableButton.TabIndex = 2;
            this.ChangeVariableButton.Text = "Редактировать";
            this.ChangeVariableButton.UseVisualStyleBackColor = true;
            this.ChangeVariableButton.Click += new System.EventHandler(this.ChangeVariableButton_Click);
            // 
            // DeleteVariableButton
            // 
            this.DeleteVariableButton.AutoSize = true;
            this.DeleteVariableButton.Location = new System.Drawing.Point(302, 272);
            this.DeleteVariableButton.Margin = new System.Windows.Forms.Padding(4);
            this.DeleteVariableButton.Name = "DeleteVariableButton";
            this.DeleteVariableButton.Size = new System.Drawing.Size(115, 39);
            this.DeleteVariableButton.TabIndex = 3;
            this.DeleteVariableButton.Text = "Удалить";
            this.DeleteVariableButton.UseVisualStyleBackColor = true;
            this.DeleteVariableButton.Click += new System.EventHandler(this.DeleteVariableButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(139, 29);
            this.label1.TabIndex = 4;
            this.label1.Text = "Переменные";
            // 
            // listBoxVariables
            // 
            this.listBoxVariables.FormattingEnabled = true;
            this.listBoxVariables.ItemHeight = 29;
            this.listBoxVariables.Location = new System.Drawing.Point(13, 32);
            this.listBoxVariables.Margin = new System.Windows.Forms.Padding(4);
            this.listBoxVariables.Name = "listBoxVariables";
            this.listBoxVariables.Size = new System.Drawing.Size(404, 207);
            this.listBoxVariables.TabIndex = 5;
            this.listBoxVariables.SelectedIndexChanged += new System.EventHandler(this.ListBoxVariables_SelectedIndexChanged);
            // 
            // btnCalc
            // 
            this.btnCalc.Location = new System.Drawing.Point(14, 496);
            this.btnCalc.Margin = new System.Windows.Forms.Padding(4);
            this.btnCalc.Name = "btnCalc";
            this.btnCalc.Size = new System.Drawing.Size(119, 34);
            this.btnCalc.TabIndex = 8;
            this.btnCalc.Text = "Посчитать";
            this.btnCalc.UseVisualStyleBackColor = true;
            this.btnCalc.Click += new System.EventHandler(this.BtnCalc_Click);
            // 
            // txtBoxMin
            // 
            this.txtBoxMin.Location = new System.Drawing.Point(533, 51);
            this.txtBoxMin.Name = "txtBoxMin";
            this.txtBoxMin.Size = new System.Drawing.Size(43, 37);
            this.txtBoxMin.TabIndex = 9;
            // 
            // txtBoxMax
            // 
            this.txtBoxMax.Location = new System.Drawing.Point(533, 84);
            this.txtBoxMax.Name = "txtBoxMax";
            this.txtBoxMax.Size = new System.Drawing.Size(43, 37);
            this.txtBoxMax.TabIndex = 10;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(479, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 29);
            this.label2.TabIndex = 11;
            this.label2.Text = "Min:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(479, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 29);
            this.label3.TabIndex = 12;
            this.label3.Text = "Max:";
            // 
            // gridResults
            // 
            this.gridResults.AllowUserToAddRows = false;
            this.gridResults.AllowUserToDeleteRows = false;
            this.gridResults.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gridResults.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridResults.Location = new System.Drawing.Point(12, 318);
            this.gridResults.Name = "gridResults";
            this.gridResults.Size = new System.Drawing.Size(717, 99);
            this.gridResults.TabIndex = 13;
            // 
            // tbResult
            // 
            this.tbResult.Location = new System.Drawing.Point(183, 490);
            this.tbResult.Multiline = true;
            this.tbResult.Name = "tbResult";
            this.tbResult.Size = new System.Drawing.Size(100, 44);
            this.tbResult.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(177, 468);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 29);
            this.label4.TabIndex = 15;
            this.label4.Text = "Результат:";
            // 
            // btnCreateRules
            // 
            this.btnCreateRules.Location = new System.Drawing.Point(458, 158);
            this.btnCreateRules.Name = "btnCreateRules";
            this.btnCreateRules.Size = new System.Drawing.Size(124, 57);
            this.btnCreateRules.TabIndex = 16;
            this.btnCreateRules.Text = "Создать правила";
            this.btnCreateRules.UseVisualStyleBackColor = true;
            this.btnCreateRules.Click += new System.EventHandler(this.BtnCreateRules_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 29F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ClientSize = new System.Drawing.Size(775, 561);
            this.Controls.Add(this.btnCreateRules);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tbResult);
            this.Controls.Add(this.gridResults);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtBoxMax);
            this.Controls.Add(this.txtBoxMin);
            this.Controls.Add(this.btnCalc);
            this.Controls.Add(this.listBoxVariables);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DeleteVariableButton);
            this.Controls.Add(this.ChangeVariableButton);
            this.Controls.Add(this.AddVariableButton);
            this.Font = new System.Drawing.Font("Candara", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Text = "FuzzyLogic";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridResults)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button AddVariableButton;
        private System.Windows.Forms.Button ChangeVariableButton;
        private System.Windows.Forms.Button DeleteVariableButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBoxVariables;
        private System.Windows.Forms.Button btnCalc;
        private System.Windows.Forms.TextBox txtBoxMin;
        private System.Windows.Forms.TextBox txtBoxMax;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView gridResults;
        private System.Windows.Forms.TextBox tbResult;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnCreateRules;
    }
}

